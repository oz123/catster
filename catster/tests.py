from django.test import Client
from django.test import TestCase


class MyTests(TestCase):

    def test_no_cats_here(self):
        c = Client()
        resp = c.get("", REMOTE_ADDR="127.0.0.1")

        self.assertEqual(resp.content, b"no cats purring here, only via ipv6")

    def test_purring_cats(self):
        c = Client()
        resp = c.get("", REMOTE_ADDR="fe80::62b6:a299:1b67:3956")

        self.assertEqual(resp.content, b"purrr ...")
