from ipaddress import ip_address


from django.http import HttpResponse
from django.template import loader


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


#def no_cats_here():
#    return HttpResponse("no cats purring here, only via ipv6")


def cute_cat(request):
    template = loader.get_template("catster/index.html")
    context = {}
    return HttpResponse(
            template.render(context, request)
            )


def index(request):
    ip = get_client_ip(request)
    #if ip_address(ip).version == 4:
    #    return no_cats_here()

    return cute_cat(request)
