"""
WSGI config for catster project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.1/howto/deployment/wsgi/
"""
from logging import StreamHandler
from requestlogger import WSGILogger, ApacheFormatter

import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'catster.settings')

app = get_wsgi_application()


app_w_logging = WSGILogger(app,
                           [StreamHandler(),],
                           ApacheFormatter(),
                           ip_header='X-Forwarded-For')

if __name__ == '__main__':
    import argparse
    import bjoern

    parser = argparse.ArgumentParser()

    parser.add_argument('--host', default='0.0.0.0')
    parser.add_argument('--port', default=8000, type=int)

    args = parser.parse_args()

    print(f'Starting server on {args.host}:{args.port}')
    bjoern.run(app_w_logging, "0.0.0.0", 8000)
