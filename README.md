Catster
-------

A simple django application to that shows an image of cat. 

The purpose of this application is to show how to deploy a Django application
on Kubernetes.

It is deployed using NGinx to server static content and bjoern as WSGI server.

An initContaier is used to run collectstatic everytime the app starts.

The application is exposed via NGinx Ingress resource.
