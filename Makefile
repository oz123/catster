SHELL = /bin/bash
VERSION ?= $(shell git describe --tags --always --dirty)
TAG ?=$(subst +,-,$(VERSION))

ifneq (,$(wildcard ./.env))
    include .env
	export
endif

shell:
	python

requirements.txt:
	pipenv requirements > $@

docker-build:
	echo $(VERSION)
	docker build -t $(REGISTRY)/$(ORG)/$(IMG):$(TAG) --build-arg VERSION=$(VERSION) -f docker/Dockerfile . 

docker-push:
	echo $(VERSION)
	docker push $(REGISTRY)/$(ORG)/$(IMG):$(TAG)
	
k8s-update-image:
	cd k8s/kustomize && kustomize edit set image reporting=*:$(TAG) && kustomize build | kubectl apply -f -

docker-push:
	docker push $(REGISTRY)/$(ORG)/$(IMG):$(TAG)


